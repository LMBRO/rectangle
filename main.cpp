/*
 * main.cpp
 *
 *  Created on: 2017-02-09
 *      Author: etudiant
 */
#include "Rectangle.h"
#include <vector>
#include <iostream>

template<typename Object, typename Comparator>
const Object & findMax(std::vector<Object> const & arr, Comparator issLessThan) {
	int maxIndex = 0;

	for (unsigned int i = 1; i < arr.size(); i++) {
		if (issLessThan(arr[maxIndex], arr[i])) {
			maxIndex = i;
		}
	}
	return arr[maxIndex];
}

class AreaCompare {
public:
	bool operator()(Rectangle const & rec1, Rectangle const & rec2) const {
		return rec1.getLength() * rec1.getWidth() < rec2.getLength() * rec2.getWidth();
	}
};
class PerimeterCompate {
public:
	bool operator()(Rectangle const & rec1, Rectangle const & rec2) const {
		return rec1.getLength() + rec1.getWidth() < rec2.getLength() + rec2.getWidth();
	}
};
int main() {
	std::vector<Rectangle> v;
	v.push_back(Rectangle { 1, 2 });
	v.push_back(Rectangle { 4, 3 });
	v.push_back(Rectangle { 1, 11 });
	v.push_back(Rectangle { 6, 3 });


	std::cout << findMax(v, AreaCompare{}) << std::endl;
	std::cout << findMax(v,PerimeterCompate{}) << std::endl;
	return 0;
}

