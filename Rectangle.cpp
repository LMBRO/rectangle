/*
 * Rectangle.cpp
 *
 *  Created on: 2017-02-09
 *      Author: etudiant
 */

#include "Rectangle.h"

Rectangle::Rectangle(unsigned int length, unsigned int width) :
		length { length }, width { width } {
}

unsigned int Rectangle::getLength() const{
	return length;
}
unsigned int Rectangle::getWidth() const{
	return width;
}
std::ostream & operator<<(std::ostream& os, Rectangle const & rec){
	os << "(Length:" << rec.getLength() << ",";
	os << "Width:" << rec.getWidth() << ")";
}
