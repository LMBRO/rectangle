/*
 * Rectangle.h
 *
 *  Created on: 2017-02-09
 *      Author: etudiant
 */

#ifndef RECTANGLE_H_
#define RECTANGLE_H_
#include "iostream"
class Rectangle{
public:
	Rectangle(unsigned int, unsigned int);
	unsigned int getWidth() const;
	unsigned int getLength() const;
	friend std::ostream & operator<<(std::ostream&, Rectangle const &);
private:
	unsigned int length;
	unsigned int width;

};



#endif /* RECTANGLE_H_ */
